/**
 * Created by jason on 4/18/16.
 */
// Have a dummy array and use that array to populate table
// Look at AJAX
var tasks = [], host = "http://www.localhost:3000/";
delFunc = function(row) {
    var del = row.parent().parent();
    var rowsList = $('tr[data-index]');
    var index = parseInt(del.data('index'));
    console.log(tasks);
    sendDelete(tasks[index]._id);
    for (i = index; i < tasks.length; i++) {
        $(rowsList[i]).attr("data-index", i - 1);
    }
    tasks.splice(index, 1);
    del.remove();
};

sendDelete = function(id) {
  $.ajax({
      url: host + id,
      type: "DELETE"
  })
};

disableRow = function(row) {
    row.addClass('done');
    row.find('.del-btn').prop('disabled', true);
};

enableRow = function(row) {
    row.removeClass('done');
    row.find('del-btn').prop('disabled', false);
};

crossOut = function(row) {
    var tr = row.parent().parent();
    if (row.prop("checked")) {
        // AVOID DOING THIS: THIS IS STILL IN-LINE CSS
        //  - Create class in CSS for this and simply add that class to this row
        // row.css({'text-decoration': 'line-through'});
        // row.addClass('done');
        // Finds an occurance of the .del-btn in the scope of the <tr>
        // row.find('.del-btn').prop('disabled', true);
        disableRow(tr);
    } else {
        enableRow(tr);
    }
};

getTaskIDs = function() {
    var ids = [];
    for (var i = 0; i < tasks.length; i++) {
        ids.push(tasks[i]._id);
    }
    return ids;
};

sendUpdatedStatuses = function() {
    $.ajax({
        url: host,
        type: "PATCH",
        data: {
            ids: getTaskIDs()
        }
    })
        .done(function() {
            console.log("Updating statuses for all tasks...");
        })
};

checkAll = function() {
    if ($('#check-all').prop("checked")) {
        $('.check').prop('checked', true);
        $('tr').addClass('done');
        $('.del-btn').prop('disabled', true);
        // Need to set status of all items in the database to true when checkall is selected
        sendUpdatedStatuses();
    } else {
        $('.check').prop('checked', false);
        $('tr').removeClass('done');
        $('.del-btn').prop('disabled', false);
        sendUpdatedStatuses();
    }


    // if ($(this).prop("checked")) {
    //     console.log("check-all checked");
    //     // Don't need to use table, you can just select by the class
    //     results.prop("checked", true);
    //     $.each(results, crossOut);
    // } else {
    //     console.log("check-all unchecked");
    //     results.prop("checked", false);
    //     $.each(results, crossOut);
    // }
};

createTask = function(task, status, dueDate) {
    return {"task": task, "status": status, "due": dueDate};
};

createButton = function(btnClass, text) {
    return $('<button>').addClass(btnClass).html(text);
};

createCheckbox = function(checkboxClass) {
    return $('<input>').addClass(checkboxClass).attr('type', 'checkbox');
};

handleAddButton = function(table) {
    var task = $('#task').val(),
        date = $('#date').val();
    sendTask(task, false, date, table);

};

createRow = function(checkbox, task, date, button, i) {
    var row = $('<tr/>'), td1 = $('<td/>').append(checkbox), td2 = $('<td/>').append(task).addClass("taskBox"),
        td3 = $('<td/>').append(date).addClass("dateBox"), td4 = $('<td/>').append(button);
    row.attr('data-index', i);
    $('#todo-table').append(row.append(td1).append(td2).append(td3).append(td4));
}

fillTable = function(taskList) {
    for (i = 0; i < taskList.length; i++) {
        createRow(createCheckbox('check'), taskList[i].task, taskList[i].due, createButton('del-btn', 'X'), i);
    }
};

update = function(id, taskObj) {
    $.ajax({
        url: host + id,
        data: {
            "task": taskObj.task,
            "status": taskObj.status,
            "due": taskObj.due
        },
        type: "PATCH",
        dataType: "json"
    })
        .done(function(json) {
            console.log(json.data);
        })
}

sendTask = function(task, completed, date, table) {
    $.ajax({
        url: host,

        data: {
            "task": task,
            "status": completed,
            "due": date
        },

        type: "POST",

        dataType: "json"
    })
        .done(function(json) {
            var data = json.data;
            createRow(createCheckbox('check'), task, date, createButton('del-btn', 'X'), tasks.length);
            tasks.push({"task": data.task, "status": data.status, "due": data.due, "_id": data._id});
        })
        .fail(function() {
            console.log("Failed to send information to server.");
        })

};

getData = function(url) {
    $.ajax({
        url: url,

        type: "GET",

        dataType: "json",

    })

    .done(function(json) {
        var array = json.data;
        for (i = 0; i < json.data.length; i++) {
            tasks.push({"task": array[i].task, "status": array[i].status,
                "due": array[i].due, "_id": array[i]._id});
        }
        fillTable(tasks);
    })

    .fail(function() {
        console.log("Failed to retrieve data from server");
    })


};

$( document ).ready(function() {
    var dummyTasks = [{"task": "Buy milk", "status": false, "due": "2016-27-04"},
        {"task": "Do homework", "status": false, "due": "2016-28-04"},
        {"task": "Bake a cake", "status": false, "due": "2016-30-04"},
        {"task": "Work on project", "status": false, "due": "2016-31-04"},
        {"task": "Read up on AJAX", "status": false, "due": "2016-22-04"}
    ];
    // fillTableWithDummy(dummyTasks);
    getData(host);
    // update("571a892acd8fe53357d5315f", {"task": "newTask", "due": "2016-24-05"});

    var table = $('#todo-table');
    //$('#add-btn').click(addButton(tasks));
    // Cannot have callback be actual function with parameters, have to wrap it in another function
    $('#add-btn').click(function() {
        handleAddButton(table, tasks);
    });

    table.on('click', '.del-btn', function() {
        delFunc($(this), tasks);
    });

    table.on('click', '.check', function() {
        crossOut($(this));
    });

    table.on('click', '.taskBox', function() {
        var originalText = $(this).html();
        var input = $('<input/>').attr('id', 'update').attr('type', 'text').attr('value', originalText);
        $(this).replaceWith($('<td/>').append(input));
    });

    table.on('click', '.dateBox', function() {
        var date = $(this).html();
        var input = $('<input/>').attr('id', 'dateUpdate').attr('type', 'date').attr('value', date);
        $(this).replaceWith($('<td/>').append(input));
    });

    table.on('blur', '#update', function() {
        var text = $('#update').val();
        var index = parseInt($(this).parent().parent().data('index'));
        var taskObj = tasks[index];
        taskObj.task = text;

        $.ajax({
                url: "host" + taskObj._id,
                data: taskObj,
                type: "PATCH",
                dataType: "json"
            })
            .done(function() {
                console.log("Update successful.");
                tasks[i] = taskObj;
                $('#update').parent().addClass("taskBox");
                $('#update').replaceWith(text);
            })
            .fail(function() {
                console.log("Failed to update server.");
            })

    });



    table.on('blur', '#dateUpdate', function() {
        var date = $(this).val();
        console.log(date);
        var i = parseInt($(this).parent().parent().data('index'));
        var taskObj = tasks[i];
        taskObj.due = date;

        $.ajax({
            url: host + taskObj._id,
            data: taskObj,
            type: "PATCH",
            dataType: "json"
        })
            .done(function() {
                console.log("Update succesful.");
                tasks[i] = taskObj;
                $('#dateUpdate').parent().addClass("dateBox");
                $('#dateUpdate').replaceWith(date);
            })
            .fail(function() {
                console.log("Failed to update server.");
            })
    });

    $('#check-all').click(checkAll);

});